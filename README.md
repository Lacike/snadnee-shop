<p align="center"><a href="https://www.snadnee.com/cs/" target="_blank"><img src="https://www.snadnee.com/assets/img/snadnee-logo.svg" width="400"></a></p>

<p align="center">
<strong>&</strong>
</p>

<div align="center">[ JUNIOR LARAVEL DEVELOPER ]</div>
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Zadanie

Vytvoriť aplikáciu pre malý obchod s ovocím a zeleninou (produkty), ktoré sa predávajú po bedničkách jednotlivým dodávateľom.

Apllikácia bude mať **CRUD** pre produkty, **CREATE** a **LIST** pre bedničky, a **LIST** pre dodávateľov.

### Backend

Použitá databáza je MySQL, ktorá pozostáva z troch základných tabuliek :

**Produkty** :

[Products]

* id
* nazov
* skladovost
* cena
* foto
* dodavatel

_Zoznam produktov optimalizujte pre 10 000 záznamov, pridajte stránkovanie,vyhľadávanie (id,názov,dodávateľ), radenie (id, čas poslednej editácie), filtrácia (skladom)._

_V detaile produktu pridať zoznam bedničiek, v ktprích sa produkt nachádza._

**Bedničky** :

[Boxes]

* id
* produkty
* zlava

_Bednička musí obsahovať aspoň 1 kus ovocia._

**Dodávatelia** :

[Supliers]

* id
* nazov

_Tento zoznam iba naseedovať._

### Frontend

Pre zobrazenie dát môže byť použitý jeden z nasledovných **CSS** frameworkov :

1. Bootstrap
1. Tailwind
1. Bulma

V mojom prípade ide o použitie bootstrap templatu pre e-commerce : [Vegefoods](https://themewagon.com/themes/free-responsive-fruits-vegetable-e-commerce-website-template-vegefoods/)

Ku kódu pridajte README súbor (práve čítate) jeho inštaláciu a dôležité funkcie. Kód primerane komentujte.
Naprogramujte **SEEDER** všetkých dát pre ľahké otestovanie funkčnosti.

Hotovú úlohu uložte verejne na GIT účet a nazdielajte odkaz : [GitLab](https://gitlab.com/Lacike/snadnee-shop)

### Inštalácia

**Naklonovanie projektu** :
```
git clone https://gitlab.com/Lacike/snadnee-shop.git
```

**Aktualizacia a stiahnutie PHP packagist** :
```
composer install/update
```

**Vytvorenie databazovej struktury** :

_Predpoklad je, ze musi existovat schema SNADNEE._
```
php artisan migrate
```

**Naplnenie databazy test datami** :

_Defaultne sa spusti SEEDER pre vsetky tabulky._

```
 php artisan db:seed
```
V pripade potreby naplnit len 1 tabulku je potrebne pouzit prikaz rozsireny o class :

* SuplierSeeder
* ProductSeeder
* BoxSeeder

```
 php artisan db:seed --class=[ClassSeeder]
```
**Spustenie samotnej aplikacie na lokalnom stroji** :
```
php artisan serve
```

### Funkcie
