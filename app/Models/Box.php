<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    use HasFactory;

    protected $fillable = ['discount'];

    /**
     * A box can have many products
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->latest('id');
    }

}
