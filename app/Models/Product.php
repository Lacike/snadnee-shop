<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name','description','stock','preis','photo','suplier_id'];

    public function suplier()
    {
        return $this->belongsTo(Suplier::class);

    }

    /**
     * A product can have many boxes
     */
    public function boxes()
    {
        return $this->belongsToMany(Box::class);
    }
}
