<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Suplier extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->hasMany(Product::class,'suplier_id');

    }

}
