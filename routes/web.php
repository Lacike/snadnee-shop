<?php

use App\Http\Controllers\ProductController;
use App\Models\Suplier;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/supliers', function () {
    $supliers = Suplier::with('products')->get();

    return view('supliers',compact('supliers'));
});

Route::resource('/products',ProductController::class);

Route::get('/boxes', function () {
    return view('boxes');
});

