<?php

namespace Database\Factories;

use App\Models\Box;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class BoxFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Box::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            // Nastavenie 70% pravdepodobnosti ze box bude bez zlavy
            'discount' => $this->faker->optional($weight = 0.3, $default = 0)->randomElement([0, 5, 10, 15, 20, 30, 40])
        ];
    }
}
