<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Suplier;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $supliers = Suplier::all()->pluck('id')->toArray();

        return [
            'name' => ucfirst($this->faker->word),
            'description' => $this->faker->text(300),
            'stock' => $this->faker->numberBetween(0, 1),
            'preis' => $this->faker->randomFloat(5, 10, 100),
            'photo' => null,
            'suplier_id' => $this->faker->randomElement($supliers)
        ];
    }
}
