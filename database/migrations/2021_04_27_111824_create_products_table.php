<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->boolean('stock');
            $table->float('preis',5,2);
            $table->string('photo')->nullable();
            $table->unsignedBigInteger('suplier_id');
            $table->foreign('suplier_id')->references('id')->on('supliers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('supliers', function (Blueprint $table) {
//            $table->dropForeign('suplier_id');
//            $table->dropColumn('suplier_id');
//        });

        Schema::dropIfExists('products');
    }
}
