<?php

namespace Database\Seeders;

use App\Models\Box;
use App\Models\Product;
use App\Models\Suplier;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SuplierSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(BoxSeeder::class);

    }
}
