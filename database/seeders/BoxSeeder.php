<?php

namespace Database\Seeders;

use App\Models\Box;
use App\Models\Product;
use Faker\Factory;
use Illuminate\Database\Seeder;

class BoxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $products = Product::all()->pluck('id')->toArray();
        $products = Product::all();
        $faker = Factory::create('sk_SK');

        // Samotne spustenie generovania dat pre vytvorenie Boxes
//            Box::factory(10)->create();

        // Vytvorenie Boxes a nasledne naplnenie boxes produktami (min=1)
        Box::factory(10)->create()->each(function ($box) use ($faker, $products) {
            $productsCount = rand(1, 5);

            for ($i = 0; $i < $productsCount; $i++) {
                $box->products()->save($faker->unique()->randomElement($products));
            }
        });

    }
}
