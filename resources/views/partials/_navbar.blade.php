<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'SNADNEE') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="{{ url('/') }}" class="nav-link">{{ __('Home') }}</a></li>
                <li class="nav-item"><a href="{{ url('/products') }}" class="nav-link">{{ __('Produkty') }}</a></li>
                <li class="nav-item"><a href="{{ url('/boxes') }}" class="nav-link">{{ __('Bedničky') }}</a></li>
                <li class="nav-item"><a href="{{ url('/supliers') }}" class="nav-link">{{ __('Dodávatelia') }}</a></li>
            </ul>
        </div>
    </div>
</nav>
