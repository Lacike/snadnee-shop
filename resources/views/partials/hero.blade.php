<div class="hero-wrap hero-bread" style="background-image: url('{{ asset('images/bg_1.jpg') }}');">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('/') }}">{{ __('Home') }}</a></span> <span class="mr-2"><a href="{{ url('/products') }}">{{ __('Produkty') }}</a></span>
                    <span>{{ __('Detail produktu') }}</span></p>
                <h1 class="mb-0 bread">{{ __('Detail produktu') }}</h1>
            </div>
        </div>
    </div>
</div>
