@extends('layouts.master')

@section('content')
    <!-- ##### Home Section ##### -->
    @include('partials.home-section')

    <!-- ##### Services Section ##### -->
    @include('partials.services-section')

    <!-- ##### Category Section ##### -->
    @include('partials.category-section')

    <!-- ##### Products Section ##### -->
    @include('partials.products-section')

    <!-- ##### Deal Section ##### -->
    @include('partials.deal-section')

    <!-- ##### Testimony Section ##### -->
    @include('partials.testimony-section')

    <hr>

    <!-- ##### Partner Section ##### -->
    @include('partials.partner-section')

    <!-- ##### Newsletter Section ##### -->
    @include('partials.newsletter-section')

@endsection
