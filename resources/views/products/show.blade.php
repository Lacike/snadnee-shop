@extends('layouts.master')

@section('content')

    <!-- ##### Hero section ##### -->
    @include('partials.hero')

    <!-- ##### Detail product ##### -->
    @include('partials.detail-product')

    <!-- ##### Related products ##### -->
    @include('partials.related-products')

    <!-- ##### Newsletter Section ##### -->
    @include('partials.newsletter-section')

@endsection
