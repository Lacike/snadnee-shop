@extends('layouts.master')

@section('content')

    <div class="hero-wrap hero-bread" style="background-image: url('{{ asset('images/bg_1.jpg') }}');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('/') }}">Home</a></span>
                        <span>{{ __('Dodávatelia') }}</span>
                    </p>
                    <h1 class="mb-0 bread">{{__('Dodávatelia') }}</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">

            <div class="row">
                @forelse ($supliers as $suplier)
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="product">
                            <a href="#" class="img-prod">
                                <img class="img-fluid" src="{{ asset('images/company-logo.jpg') }}"
                                     alt="{{ $suplier->company }}">
                                @if (count($suplier->products) > 0)
                                    <span class="status"> {{ count($suplier->products) }} produkt</span>
                                @endif

                                <div class="overlay"></div>
                            </a>
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="#">{{ $suplier->company }}</a></h3>
                                @if (count($suplier->products) > 0)
                                    <div class="d-flex">
                                        <div class="pricing">
                                            <p>Počet produktov : <span>{{ count($suplier->products) }}</span>
                                            </p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                @empty
                    <p>Je nám ľúto, žiaľ ešte neexistuje žiadny dodávateľ.</p>
                @endforelse

            </div>

        @if (count($supliers) > 12)
            <!-- ##### Paginations Section ##### -->
                @include('partials.paginations')
            @endif

        </div>
    </section>

    <!-- ##### Newsletter Section ##### -->
    @include('partials.newsletter-section')

@endsection
